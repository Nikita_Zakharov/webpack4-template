function prependSassUtils() {
    const utils = [
        '@import "./assets/scss/utils/variables";',
        '@import "./assets/scss/utils/mixins";'
    ]

    return utils.join(' ')
}

module.exports = prependSassUtils