// Webpack4
const path = require('path')
const fs = require('fs')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const prependSassUtils = require('./prependSassUtils')

function generateHtmlPlugins() {
    // Read files in template directory
    const templateFiles = fs.readdirSync(path.resolve(__dirname, 'html'))
    return templateFiles.map(item => {
        // Split names and extension
        const parts = item.split('.')
        const name = parts[0]
        const extension = parts[1]
        // Create new HTMLWebpackPlugin with options
        return new HtmlWebpackPlugin({
            filename: `${name}.html`,
            template: `./html/${name}.${extension}`,
            inject: false
        })
    })
}

const generatedHtmlFiles = generateHtmlPlugins()

module.exports = {
    entry: {main: './js/app.js'},
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.js',
        // publicPath: '',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.s?css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                url: false
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                outputStyle: 'expanded',
                                data: prependSassUtils()
                            }
                        }
                    ]
                })
            },
            {
                test: /\.(woff|woff2|ttf|eot)$/,
                loader: 'file-loader'
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            // outputPath: 'img/',
                            // publicPath: ''
                        }
                    }
                ],
            },
            {
                test: /\.html$/,
                use: ['html-loader']
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({filename: 'style.css'}),
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin([
            {
                from: './assets/images',
                to: 'assets/images'
            },
            {
                from: './assets/fonts',
                to: 'assets/fonts'
            }
        ]),
    ]
        .concat(generatedHtmlFiles),
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        historyApiFallback: true,
        port: 9000
        // host: '0.0.0.0'
    }
}
